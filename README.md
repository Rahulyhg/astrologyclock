# Astrology Clock

Displays the time and sun sign.

Uses Astro font type by Cosmorama.

    git clone git@github.com:robertmermet/astrologyclock.git

>**demo** [robertmermet.com/projects/astrologyclock](http://robertmermet.com/projects/astrologyclock)
